function toCamelCase(){
	let str = prompt("Потрібно замінити: ");
  let result1 = str.replace(/_./gi, str => str.toUpperCase());
  let result2 = result1.replace(/_/gi, "");
  alert(result2);
}
function toSnakeCase(){
	let str=prompt("Потрібно замінити: ");
  let x = str.replace(/([A-Z])/g,"_$1");
  let x1 = x.replace(/[A-Z]/g, str => str.toLowerCase());
  alert(x1);
}
function task2_3(){
 let text=prompt("Введіть текст: ");
 //"jjyuc ug 3456/05/29 /jyf hh/hfc/gg kjggy/gg/gg jcyjbdsjbnsjrb 4444/12/09/3 kjdvns 4444/44/44";
 let res = text.replace(/\d\d\d\d\/[0-1]\d\/[0-3]\d/g,(text.match(/\d\d\d\d\/[0-1]\d\/[0-3]\d/))[0].replace(/\//g, "."));
 alert(res);
}

function task3_1(){
	let a = Number(prompt("Введіть число: "));
  let k = new Array();
  for(let i=1;i<=100;i++){
  	if(a%i==0){
    	k.push(i)
    }
  }
  alert("Числа (від 1 до 100) кратні "+ a+" :"+k);
}
function task3_2(){
		let min = Number(prompt("Введіть мінімальне число діапазону: "));
    let max = Number(prompt("Введіть максимальне число діапазону: "));
    let k = new Array();
    for(let i=min;i<=max;i++){
    	if((i-min+1)%4==0){
      	k.push(i);
      }
    }
    alert("кожний 4 елемент діапазону від "+min + " до "+ max+" : (включно) "+k);
}
function task3_3(){
		let a = Number(prompt("Введіть ціле число A: "));
    let b = Number(prompt("Введіть ціле число B: "));
    if(a>b){let c=a;a=b;b=c;}
    let k = new Array();
    let sum =0;
    for(let i=a;i<=b;i++){
    	k.push(i);
      sum+=i;
    }
    alert("Числа від "+a+" до "+b+": "+k+"\nЇх сумма: "+sum+"\nЇх кількість: "+ k.length);
}
function task3_4(){
	let a = Number(prompt("Введіть ціле число: "));
  let k = new Array();
  for(let i=a;i>=0;i--){
  	k.push(i);
  }
  alert("Числа від "+a+" до 0: "+k);
}
function task3_5(){
	let a = Number(prompt("Введіть ціле число: "));
	let b = Number(prompt("Введіть цілу степінь: "));
  let k = new Array();
  let aa=a;
    if(b==0){alert(aa+" в степені "+b+" = 1");}else{
    for(let i=1;i<b;i++){
      a*=aa;
    }
    alert(aa+" в степені "+b+" = "+a);
	}
}
function task3_6(){
	let a = Number(prompt("Введіть число: "));
  let max=a;
  while(true){
  	let b = Number(prompt("Введіть число: "));
    if(max<b){max=b;}
    if(a==b){break;}
    a=b;
  }
  alert("Максимальне з введених: "+ max);
}
function task3_7(){
	let kr = ["Понеділок","Вівторок","Середа","Четвер","П'ятниця","Субота","Неділя"];
  let ok;
  while(true){
  	for(let i=0;i<7;i++){
    	ok = prompt(kr[i]+" Хочете побачити наступний день? (напишіть OK (на англійській мові, великими літерами)): ");
      if(ok!="OK"){break;}
    }
    if(ok!="OK"){break;}
  }
}
function task3_8(){
	let k = new Array();
	for(let i=1;i<=10;i++){
  	let b="";
  	for(let j=2;j<=9;j++){
    	b=b+j+"x"+i+"="+(i*j)+"  ";
    }
    b=b+"\n";
    k.push(b);
  }
  
  alert(k[0]+k[1]+k[2]+k[3]+k[4]+k[5]+k[6]+k[7]+k[8]+k[9]);
}